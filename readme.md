# pip 设置源（推荐）
pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
pip config set install.trusted-host mirrors.aliyun.com


### 生成 requirements.txt
```
pip freeze > requirements.txt
```
### 安装依赖
```angular2html
pip install -r requirements.txt
pip install -r requirements.txt --no-cache-dir

```
### 卸载依赖
```angular2html
pip uninstall package_name
```

### 依赖打包成离线安装包
```angular2html
pip download -r requirements.txt -d ./offline-packages
pip wheel -r requirements.txt --wheel-dir ./offline-packages
```
### 离线安装依赖
```angular2html
pip install ./offline-packages/package_name-version.tar.gz

pip install --no-index --find-links=./offline-installation -r requirements.txt
```