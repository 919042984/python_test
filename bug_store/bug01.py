import tkinter as tk
from ttkbootstrap import Style
#
def main():
    # 创建主窗口
    root = tk.Tk()

    # 设置全屏模式
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    root.geometry(f"{screen_width}x{screen_height}+0+0")
    root.state('zoomed')  # 可以尝试使用此命令替代上面的geometry设置，实现全屏最大化

    # 选择并应用 ttkbootstrap 皮肤
    style = Style(theme='superhero')  # 更换为你喜欢的主题名称，如 'flatly', 'darkly', 'cyborg' 等

    # 添加界面元素（此处仅作示例，根据实际需求设计）
    container = tk.Frame(root)
    container.pack(fill=tk.BOTH, expand=True)

    label = tk.Label(container, text="这是一个全屏且美观的Tkinter界面", font=("Arial", 24))
    label.pack(pady=20)

    label = tk.Label(container, text="这是一个全屏且美观的Tkinter界面", font=("Arial", 24))
    label.pack(pady=40)

    # tkinter 画一个表格
    table = tk.Frame(container)
    table.pack(pady=20)

    # 表格第一行 列名
    col1 = tk.Label(table, text="姓名", font=("Arial", 14))
    col1.grid(row=0, column=0, padx=10, pady=10)

    col2 = tk.Label(table, text="年龄", font=("Arial", 14))
    col2.grid(row=0, column=1, padx=10, pady=10)

    col3 = tk.Label(table, text="性别", font=("Arial", 14))
    col3.grid(row=0, column=2, padx=10, pady=10)


    # 显示窗口

    # 运行主循环
    root.mainloop()

if __name__ == "__main__":
    main()
