import datetime







# 模拟获取系统CPU利用率
def get_cpu_usage():
    # 在实际应用中，这里会调用系统监控库或API获取数据
    return 75.3  # 假设当前CPU使用率为75.3%

# 模拟解析日志文件，查找错误信息
def search_error_logs(log_file):
    # 实际应用中，这里会打开并解析实际日志文件
    fake_log_entries = [
        "2024-04-15 1.jpg: [INFO] Service started successfully",
        "2024-04-15 13:45:32,789 - myapp.module - WARNING - Some non-critical event occurred",
        "2024-04-15 14:2.png: [ERROR] Unexpected error: Division by zero",
    ]
    error_lines = [line for line in fake_log_entries if "[ERROR]" in line]
    return error_lines

# 模拟重启服务
def restart_service(service_name):
    print(f"Attempting to restart service '{service_name}'...")
    # 在实际应用中，这里会调用系统服务管理工具（如systemctl、service、psutil等）
    print("Service restarted successfully. (Simulation)")

# 主循环，模拟交互式运维会话
while True:
    print("\nInteractive Ops Demo:")
    print("1. Check CPU usage")
    print("2. Search error logs")
    print("3. Restart a service")
    print("4. Exit")

    choice = input("Enter your choice (1-4): ")

    if choice == '1':
        cpu_usage = get_cpu_usage()
        print(f"Current CPU usage: {cpu_usage}%")
    elif choice == '2':
        log_file = input("Enter the path to the log file: ")
        errors = search_error_logs(log_file)
        if errors:
            print("Error log entries found:")
            for line in errors:
                print(line)
        else:
            print("No error log entries found.")
    elif choice == '3':
        service_name = input("Enter the name of the service to restart: ")
        restart_service(service_name)
    elif choice == '4':
        print("Exiting demo...")
        break
    else:
        print("Invalid choice. Please enter a number between 1 and 4.")

    print("\n--- Press Enter to continue ---")
    input()  # Wait for user to press Enter before proceeding to the next operation



# 修改文件夹名称
# 1. 打开文件所在目录
# 2. 输入新的文件夹名称
# 3. 按下回车键，确认修改
# 4. 输入新的文件夹路径，确认修改
# 5. 确认修改后，关闭文件 explorer