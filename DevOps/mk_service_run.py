import os

base_dir = os.path.dirname(os.path.abspath(__file__))

old_folder_path = base_dir + "/server"
new_folder_path = old_folder_path + input("备份文件夹后缀: ")


# 检查文件夹是否存在
def check_folder_exist(folder_path):
    if os.path.exists(folder_path):
        return True
    else:
        return False

    # 修改文件夹名称


def update_dir_name():
    try:

        if not check_folder_exist(old_folder_path):
            print(f"需要备份的文件夹不存在: {old_folder_path}")
            return
        os.rename(old_folder_path, new_folder_path)
        print(f"Folder renamed successfully.")
    except Exception as e:
        print(f"修改文件夹名称是发生错误: {str(e)}")


# 解压.tar.gz文件至指定目录
def extract_tar_gz(tar_gz_file_path, extract_dir_path):
    try:
        os.system(f"tar -zxvf {tar_gz_file_path} -C {extract_dir_path}")
        print(f"File extracted successfully.")
    except Exception as e:
        print(f"An error occurred while extracting the file: {str(e)}")


# 获取当前文件夹目录并把目录地址赋值给全局变量
# current_dir = os.path.dirname(os.path.abspath(__file__))
print(old_folder_path)
print(new_folder_path)

update_dir_name()
