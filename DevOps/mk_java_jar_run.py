## 写一个tar.gz 文件解压到指定目录并运行java jar包

import os
import tarfile

# tar_gz_file = 'F:/V53.tar.gz'
# extract_dir = 'F:/tmp/test'
# 获取当前时间
import time

now_time = time.strftime('%Y-%m-%d', time.localtime(time.time()))


# 解压tar.gz文件到指定目录
def extract_tar_gz(src_dir, target_dir):
    if os.path.exists(target_dir):
        os.system('rm -rf %s' % target_dir)
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    with tarfile.open(src_dir, 'r:gz') as tar:
        tar.extractall(target_dir)


# 备份文件夹
def backup_dir(src_dir, target_dir='F:/tmp/test_bak'):
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)

    for file in os.listdir(src_dir):
        src_file = os.path.join(src_dir, file)
        dst_file = os.path.join(target_dir, file)
        if os.path.isfile(src_file):
            if not os.path.exists(target_dir): os.makedirs(target_dir)
            if not os.path.exists(dst_file) or os.path.getsize(src_file) != os.path.getsize(dst_file):
                open(dst_file, "wb").write(open(src_file, "rb").read())  # 覆盖写入


# 解压文件夹下面的全部tar.gz文件
def extract_all_tar_gz(extract_dir):
    for file in os.listdir(extract_dir):
        if file.endswith('.tar.gz'):
            tar_gz_file = os.path.join(extract_dir, file)
            extract_tar_gz(tar_gz_file)


# 写一个
if __name__ == '__main__':
    # 解压tarextract_tar_gz(tar_gz_file, extract_dir)
    # backup_dir("F:/tmp/test/MKPaaS-app", "F:/tmp/test_bak/MKPaaS-app-bak")
    extract_tar_gz("F:/V53.tar.gz", 'F:/tmp/test')

