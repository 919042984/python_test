import pandas as pd
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, Text, inspect, text, Float
from sqlalchemy.orm import sessionmaker, declarative_base

# 创建 SQLAlchemy 基础对象
Base = declarative_base()


# 定义一个通用的 Excel 列转 SQLAlchemy 列的函数
def excel_column_to_sqlalchemy_column(name, dtype):
    column_type_map = {
        'int64': Integer,
        'float64': Float,
        'object': String(255),  # 为 VARCHAR 类型指定默认长度（例如 255）
        # 'datetime64[ns]': DateTime,
        # ... 添加其他类型映射，如 Date、Time 等
    }

    sqlalchemy_dtype = column_type_map.get(str(dtype), Text)
    return Column(name, sqlalchemy_dtype)


# 假设 Excel 文件中第一行为表头，第二行开始为数据
def create_tables_and_insert_data(excel_path, mysql_url, engine=None):
    # 读取 Excel 文件，获取所有 sheet 名称及对应的数据
    all_sheets = pd.read_excel(excel_path, sheet_name=None)

    # 初始化 SQLAlchemy Engine（如果未提供）
    if engine is None:
        engine = create_engine(mysql_url)

    # 创建 Session
    Session = sessionmaker(bind=engine)
    session = Session()

    for sheet_name, df in all_sheets.items():
        # 检查表是否存在，如果存在则清空数据（这里假设要覆盖原有数据）
        insp = inspect(engine)
        if sheet_name in insp.get_table_names():
            delete_statement = text(f'DELETE FROM `{sheet_name}`')
            session.execute(delete_statement)
            session.commit()

        # 移除列名中的空格
        df.columns = [col.strip() for col in df.columns]

        # 创建表结构
        metadata = MetaData()
        table = Table(
            sheet_name,
            metadata,
            *[excel_column_to_sqlalchemy_column(name, dtype) for name, dtype in zip(df.columns, df.dtypes)],
        )
        table.create(engine, checkfirst=True)

        # 插入数据
        df.to_sql(sheet_name, con=engine, if_exists='append', index=False)

    session.commit()


mysql_url = 'mysql+pymysql://root:123456@localhost/cm?charset=utf8mb4'
excel_path = '/Users/cm/Desktop/报障单.xlsx'
# 使用示例
# mysql_url = 'mysql+pymysql://username:password@localhost/dbname?charset=utf8mb4'
# excel_path = 'path_to_your_excel_file.xlsx'

create_tables_and_insert_data(excel_path, mysql_url)
