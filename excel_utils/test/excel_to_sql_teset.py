import unittest

from excel_utils.excel_to_sql import TableCreator


class TestTableCreator(unittest.TestCase):

    def setUp(self):
        # 此方法在每个测试方法执行前执行，用来设置测试环境
        # 创建TableCreator实例，用于在测试中使用
        self.table_creator = TableCreator(excel_path="/Users/cm/Desktop/test.xlsx", table_name="test_table",
                                          if_not_exists=False)

    def test_init(self):
        # 测试__init__方法
        self.assertEqual(self.table_creator.excel_path, "/Users/cm/Desktop/test.xlsx")
        self.assertEqual(self.table_creator.table_name, "test_table")
        self.assertFalse(self.table_creator.if_not_exists)

    def test_generate_mysql_create_table_sql(self):
        # 测试generate_mysql_create_table_sql方法
        sql = self.table_creator.generate_mysql_create_table_sql()
        self.assertTrue("CREATE TABLE" in sql)
        self.assertTrue("`test_table`" in sql)

    def test_generate_mysql_insert_sqls1(self):
        # 测试generate_mysql_insert_sqls1方法
        insert_sqls, values = self.table_creator.generate_mysql_insert_sqls1()
        self.assertIsInstance(insert_sqls, list)
        self.assertIsInstance(values, list)
        self.assertEqual(len(insert_sqls), len(values))  # SQL语句数量和数据行数量应一致

    def test_generate_mysql_insert_sqls(self):
        # 测试generate_mysql_insert_sqls方法
        insert_sqls, values = self.table_creator.generate_mysql_insert_sqls()
        self.assertIsInstance(insert_sqls, list)
        self.assertIsInstance(values, list)
        self.assertEqual(len(insert_sqls), len(values))  # SQL语句数量和数据行数量应一致

    def test_show_mysql_insert_sqls(self):
        # 测试show_mysql_insert_sqls方法
        sqls = self.table_creator.show_mysql_insert_sqls()
        self.assertIsInstance(sqls, list)
        # 验证是否每个插入SQL都已经被正确格式化
        for sql in sqls:
            self.assertTrue("%s" not in sql)  # 如果%s存在，则说明SQL未正确格式化


if __name__ == '__main__':
    unittest.main()
