import logging
import re

import pandas as pd
from pypinyin import lazy_pinyin
from sqlalchemy import text


def chinese_to_pinyin(text: str = '') -> str:
    """
    将中文文本转换为拼音。

    参数:
    text: str - 需要转换为拼音的中文文本，默认为空字符串。

    返回值:
    str - 转换后的拼音文本。
    """
    try:
        return_string = []
        pattern = r'[^\u4e00-\u9fa5\s]'  # 更新正则表达式，保留空格
        for char in re.sub(pattern, '', text):
            if '\u4e00' <= char <= '\u9fa5':
                # 增加异常处理
                pinyin_list = lazy_pinyin(char)
                if pinyin_list:  # 确保列表非空
                    return_string.append(pinyin_list[0])
            else:
                return_string.append(char)
        return "_".join(return_string)
    except Exception as e:
        print(f"转换失败: {e}")
        return ""  # 或者根据实际情况返回一个错误标记或内容


# 自定义数据类型映射（可选）
data_types = {
    # 'id': 'INT',  # 整型
    # 'column2': 'DATE',  # 日期
    # 'column3': 'DECIMAL(10,2)',  # 数字
}


class TableCreator:
    def __init__(self, excel_path: str, table_name: str, if_not_exists: bool = True):
        """
               类构造函数：初始化Excel数据表操作对象
               参数:
               excel_path: str - Excel文件的路径
               table_name: str - Excel中工作表的名称
               if_not_exists: bool - 如果表不存在时的处理方式，默认为False。如果为True，则在表不存在时创建新表；如果为False，则抛出异常。

               返回值:
               无
        """
        self.excel_path = excel_path
        self.table_name = table_name
        self.if_not_exists = if_not_exists
        self.data_types_map = data_types  # 假设这是之前定义的映射
        self.logger = logging.getLogger(__name__)

    def generate_mysql_create_table_sql(self) -> str:
        """
        生成Mysql创建表的SQL语句。

        该方法不接受任何参数。

        返回值:
            string: 返回一个表示创建表的SQL语句的字符串。
        """
        try:
            df = pd.read_excel(self.excel_path, header=0, nrows=0)
            columns = df.columns.tolist()

            column_definitions = []
            for column_name in columns:
                custom_type = self.data_types_map.get(column_name)
                data_type = custom_type if custom_type is not None else 'VARCHAR(255)'
                column_definitions.append(f"{column_name} {data_type}  \n \t ")

            if_not_exists_clause = 'IF NOT EXISTS' if self.if_not_exists else ''
            create_table_sql = f"CREATE TABLE  {if_not_exists_clause}  `{self.table_name}`   \n ( {','.join(column_definitions)}\n);"
            return create_table_sql
        except Exception as e:
            print(f"创建表SQL语句失败: {e}")
            return ""  # 或者根据实际情况返回一个错误标记或内容

    def generate_mysql_insert_sqls(self) -> tuple[list[str], list[tuple]]:
        """
        生成Mysql插入数据表的SQL语句。

        该方法不接受任何参数。

        返回值:
            list: 返回一个包含多个表示插入数据表的SQL语句的字符串的列表。
        """
        insert_sqls = []
        values = []
        try:
            df = pd.read_excel(self.excel_path, header=0)
            if df.empty:
                self.logger.warning("Excel文件为空，无法生成插入语句。")
                return insert_sqls, values

            columns = df.columns.tolist()
            values_list = df.values.tolist()

            for row in values_list:
                placeholders = ', '.join(['%s' for _ in range(len(row))])
                insert_sql = f"INSERT INTO `{self.table_name}` ({','.join(columns)}) VALUES ({placeholders})"
                insert_sqls.append(insert_sql)
                # 把row中的元素左右添加单引号，以适应mysql的字符串格式
                row = [f"'{x}'" if isinstance(x, str) else x for x in row]
                values.append(tuple(row))
            return insert_sqls, values
        except FileNotFoundError:
            self.logger.error(f"文件 {self.excel_path} 未找到。")
        except pd.errors.EmptyDataError:
            self.logger.error(f"Excel文件 {self.excel_path} 为空。")
        except Exception as e:
            self.logger.error(f"处理Excel文件 {self.excel_path} 时发生错误: {e}")
        return insert_sqls, values

    def show_mysql_insert_sqls(self) -> list:
        """
        打印Mysql插入数据表的SQL语句。

        该方法不接受任何参数。
        返回值:
            无
        """
        sqls = []
        insert_sqls, values = self.generate_mysql_insert_sqls()
        for i in range(len(insert_sqls)):
            sql = insert_sqls[i] % (values[i])
            sqls.append(sql)
            print(sql)
        return sqls

# 示例：配置日志
# logging.basicConfig(level=logging.ERROR, format='%(asctime)s - %(levelname)s - %(message)s')
#
# exc = TableCreator('/Users/cm/Desktop/test.xlsx', 'example_table', if_not_exists=True)
# exc.show_mysql_insert_sqls()
