import pandas as pd
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, Text, inspect, Float, DateTime, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# 创建 SQLAlchemy 基础对象
Base = declarative_base()


# 定义一个通用的 Excel 列转 SQLAlchemy 列的函数
def excel_column_to_sqlalchemy_column(name, dtype):
    column_type_map = {
        'int64': Integer,
        'float64': Float,
        'object': String,
        'datetime64[ns]': DateTime,
        # ... 添加其他类型映射，如 Date、Time 等
    }

    sqlalchemy_dtype = column_type_map.get(str(dtype), Text)
    return Column(name, sqlalchemy_dtype)


# 假设 Excel 文件中第一行为表头，第二行开始为数据
def create_tables_and_insert_data(excel_path, db_url, engine=None):
    # 读取 Excel 文件，获取所有 sheet 名称及对应的数据
    all_sheets = pd.read_excel(excel_path, sheet_name=None)

    # 初始化 SQLAlchemy Engine（如果未提供）
    if engine is None:
        engine = create_engine(db_url)

    # 创建 Session
    Session = sessionmaker(bind=engine)
    session = Session()

    for sheet_name, df in all_sheets.items():
        # 检查表是否存在，如果存在则清空数据（这里假设要覆盖原有数据）
        insp = inspect(engine)
        if sheet_name in insp.get_table_names():
            delete_statement = text(f'DELETE FROM {sheet_name}')
            session.execute(delete_statement)
            session.commit()

        # 创建表结构
        metadata = MetaData()
        table = Table(
            sheet_name,
            metadata,
            *[excel_column_to_sqlalchemy_column(name, dtype) for name, dtype in zip(df.columns, df.dtypes)],
        )
        table.create(engine, checkfirst=True)

        # 插入数据
        for _, row in df.iterrows():
            insert_statement = table.insert().values(**row.to_dict())
            session.execute(insert_statement)

    session.commit()


# 使用示例
db_url = 'sqlite:///example.db'
excel_path = '/Users/cm/Desktop/test.xlsx'
create_tables_and_insert_data(excel_path, db_url)
