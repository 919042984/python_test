from db_connection_utils.mysql_connection import MySQLConnectionManager
from excel_utils.excel_to_sql import TableCreator


def excel_to_mysql_db():
    """
    This function converts an Excel file to a SQLite3 database.
    """
    connection_manager = MySQLConnectionManager(host="localhost",
                                                user="root",
                                                password="123456",
                                                database="cm",
                                                port=3306)

    connection_manager.connect()

    tbl_creator = TableCreator(excel_path="/Users/cm/Desktop/test.xlsx", table_name="test_table")

    connection_manager.executeNonQuery(tbl_creator.generate_mysql_create_table_sql())
    sqls = tbl_creator.show_mysql_insert_sqls()

    for sql in sqls:
        connection_manager.executeNonQuery(sql)
    connection_manager.disconnect()


if __name__ == '__main__':
    excel_to_mysql_db()
