import eel

eel.init('web')  # 设置 web 目录为静态文件目录（包含 index.html）

@eel.expose  # 将该函数暴露给 JavaScript 调用
def generate_new_greeting():
    return "Hello, Eel!"

if __name__ == '__main__':
    eel.init('web')
    eel.start('index.html', size=(1000, 800))  # 启动应用，指定初始页面和窗口大小
