# 通过Flask框架实现的Web Demo

# import cv2
from flask import Flask, render_template

app = Flask(__name__)  # 创建Flask实例


@app.route('/')
def index():
    # 读取同目录下的index.html文件
    return render_template('index.html')  # 加载index.html页面


if __name__ == '__main__':
    app.run(debug=True)  # 启动Flask服务
