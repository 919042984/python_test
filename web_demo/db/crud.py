# # crud.py
# from database import db_session
# from models import User
#
#
# def create_user(username, email):
#     new_user = User(username=username, email=email)
#     db_session.add(new_user)
#     db_session.commit()
#     return new_user
#
#
# def get_user_by_id(user_id):
#     return db_session.query(User).get(user_id)
#
#
# def get_users():
#     return db_session.query(User).all()
#
#
# def update_user(user_id, username=None, email=None):
#     user = get_user_by_id(user_id)
#     if username is not None:
#         user.username = username
#     if email is not None:
#         user.email = email
#     db_session.commit()
#     return user
#
#
# def delete_user(user_id):
#     user = get_user_by_id(user_id)
#     db_session.delete(user)
#     db_session.commit()
