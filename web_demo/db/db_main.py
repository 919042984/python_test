from crud import create_user, get_user_by_id, get_users, update_user, delete_user

# 新增用户
new_user = create_user("Alice", "alice@example.com")

# 查询用户
user = get_user_by_id(1)
print(user)

# 更新用户信息
update_user(1, username="Bob")

# 获取所有用户
users = get_users()
for user in users:
    print(user)

# 删除用户
delete_user(1)

if __name__ == '__main__':
    # 新增用户
    new_user = create_user("Alice", "alice@example.com")

    # 查询用户
    user = get_user_by_id(1)
    print(user)

    # 更新用户信息
    update_user(1, username="Bob")

    # 获取所有用户
    users = get_users()
    for user in users:
        print(user)

    # 删除用户
    # delete_user(1)
