import mysql.connector

# 定义数据库连接参数
db_config = {
    'host': '127.0.0.1',
    'port': '3307',  # 默认为3306，如果未指定可省略此键
    'user': 'root',
    'password': '123456',
    'database': 'cmxx'
}


def print_hi():
    # 建立数据库连接
    connection = mysql.connector.connect(**db_config)
    # 创建游标对象
    cursor = connection.cursor()
    # 编写SQL查询语句（例如：查询users表中所有列的数据）
    query = """SELECT * FROM sheet1;"""

    # 执行查询
    cursor.execute(query)
    # 获取查询结果
    result = cursor.fetchall()

    # 查询数据
    aaaQuery = """SELECT * FROM aa;"""
    # 执行查询
    cursor.execute(aaaQuery)
    # 获取查询结果
    result1 = cursor.fetchall()
    dict_data = dict(result1)
    print(dict_data)

    # aaMap ={}
    # for e in result1:
    #     aaMap['key'] =e[0]
    #     aaMap['value'] = e[1]
    #
    # print(aaMap)
    #
    #
    # # 遍历并打印查询结果
    # for row in result:
    #     for field in row:
    #         print(field)

    # 关闭游标和连接
    cursor.close()
    connection.close()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
