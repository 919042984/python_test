import pandas as pd

empt_dict = {'综合部': '综合管理部',
             '规划中心': '规划研究中心',
             '能源中心': '能源电力经济研究中心',
             '数智中心': '数字智能研究中心',
             '项目中心': '项目投资管理中心',
             '技经中心': '技术经济中心',
             '设计公司': '设计院',
             '管控中心': '基建安全质量管控中心',
             '科发部': '科技发展部',
             '评审中心': '评审中心',
             '党建部': '党委党建部',
             '储能研发中心': '电力新型储能多场景融合技术研发中心',
             '各部门、中心': '综合管理部;规划研究中心;能源电力经济研究中心;数字智能研究中心;项目投资管理中心;技术经济中心;设计院;基建安全质量管控中心;科技发展部;评审中心;党委党建部;电力新型储能多场景融合技术研发中心'
             }

fenhao = ';'
var11 = '\n'
# Excel文件路径
excel_file_path = "F:\\test.xlsx"  # 替换为实际的Excel文件路径


def zidian():
    str = ''
    for key, value in empt_dict.items():
        str += value + fenhao
    print(str)


def test():
    # 读取Excel文件
    df = pd.read_excel(excel_file_path)
    # 遍历DataFrame的每一行，对某些列进行更新（这里仅作示例，根据实际需求修改）
    for index, row in df.iterrows():
        # 假设我们想将年龄（假设列名为'age'）加1
        if '牵头部门' in df.columns:
            a = row['牵头部门']
            str = ''
            for inte in a.split(var11):
                if inte in empt_dict:
                    str += empt_dict[inte] + fenhao
            # 去掉最后的分号
            df.at[index, '牵头部门'] = str.rstrip(fenhao)

        if '责任人' in df.columns:
            df.at[index, '责任人'] = row['责任人'].replace("\n", ";")

        if '填报人' in df.columns:
            df.at[index, '填报人'] = row['填报人'].replace("\n", ";")

        if '配合部门' in df.columns:
            a = row['配合部门']
            str = ''
            if a == '各部门、中心':
                df.at[index, '配合部门'] = empt_dict[a]
                continue

            for inte in a.split(var11):
                # 判断元组是否存在改key
                if inte in empt_dict:
                    str += empt_dict[inte] + fenhao
            # 去掉最后的分号
            df.at[index, '配合部门'] = str.rstrip(fenhao)

    # 将更新后的数据写回原Excel文件
    df.to_excel(excel_file_path, index=False)

    print("成功")


if __name__ == '__main__':
    test()
