class User:
    # 类属性
    default_age = 18

    def __init__(self, name, age=None):
        """
        构造方法: 初始化User实例
        """
        self.name = name
        self.age = age or self.default_age

    def introduce_self(self):
        """
        实例方法: 打印用户自我介绍
        """
        print(f"My name is {self.name}, and I am {self.age} years old.")

    @classmethod
    def from_string(cls, user_str):
        """
        类方法: 从字符串创建User实例
        """
        name, age = user_str.split(',')
        return cls(name.strip(), int(age))

    @staticmethod
    def validate_age(age):
        """
        静态方法: 验证年龄是否合法（仅示例，实际验证逻辑可能更复杂）
        """
        return 0 < age <= 120

    @property
    def age(self):
        """
        属性 getter: 获取用户年龄
        """
        return self._age

    @age.setter
    def age(self, value):
        """
        属性 setter: 设置用户年龄，同时进行有效性验证
        """
        if not User.validate_age(value):
            raise ValueError("Invalid age")
        self._age = value

    def __str__(self):
        """
        特殊方法: 返回用户友好的字符串表示
        """
        return f"{self.name} ({self.age})"

    def __repr__(self):
        """
        特殊方法: 返回面向程序员的字符串表示
        """
        return f"User(name={self.name!r}, age={self.age})"


# 实例化User类
user1 = User("Alice", 25)
user2 = User.from_string("Bob, 30")

# 调用实例方法
user1.introduce_self()

# 访问和修改属性
print(user1.age)  # 输出: 25
user1.age = 30
print(user1.age)  # 输出: 30

# 尝试设置无效年龄
try:
    user1.age = -1
except ValueError as e:
    print(e)  # 输出: Invalid age

# 使用特殊方法
print(user1)  # 输出: Alice (30)
print(repr(user2))  # 输出: User(name='Bob', age=30)

# 调用静态方法
print(User.validate_age(40))  # 输出: True
print(User.validate_age(150))  # 输出: False
