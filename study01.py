# 无参方法


def demo1():
    print("无参方法")


def demo2(fileName=''):
    print(fileName)


# def dayUP(df):
#     dayup = 1
#     for i in range(365):
#         if i % 7 in [6,0]:
#             dayup = dayup*(1 - 0.01)
#         else:
#             dayup = dayup*(1 + df)
#     return dayup
# dayfactor = 0.01
# while dayUP(dayfactor) < 37.78:
#     dayfactor += 0.001
# print("工作日的努力参数是: {:.3f}".format(dayfactor))

# 入口方法 1 2 3 4 5 6 7 8 9 10
if __name__ == '__main__':
    n = 10
    for i in range(1, n + 1, 2):
        print("{0:^{1}}".format('*' * i, n))
